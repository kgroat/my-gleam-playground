# my_gleam_playground

[![Package Version](https://img.shields.io/hexpm/v/my_gleam_playground)](https://hex.pm/packages/my_gleam_playground)
[![Hex Docs](https://img.shields.io/badge/hex-docs-ffaff3)](https://hexdocs.pm/my_gleam_playground/)

```sh
gleam add my_gleam_playground
```
```gleam
import my_gleam_playground

pub fn main() {
  // TODO: An example of the project in use
}
```

Further documentation can be found at <https://hexdocs.pm/my_gleam_playground>.

## Development

```sh
gleam run   # Run the project
gleam test  # Run the tests
gleam shell # Run an Erlang shell
```
