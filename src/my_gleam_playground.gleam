import gleam/erlang/process
import gleam/io
import gleam/list
import gleam/otp/actor

pub fn main() {
  let actors = [
    actor.start(Nil, handler),
    actor.start(Nil, handler),
    actor.start(Nil, handler),
    actor.start(Nil, handler),
    actor.start(Nil, handler),
    actor.start(Nil, handler),
    actor.start(Nil, handler),
    actor.start(Nil, handler),
    actor.start(Nil, handler),
    actor.start(Nil, handler),
  ]

  let subjects =
    list.index_map(actors, fn(actor_result, _idx) {
      case actor_result {
        Ok(a) -> {
          let sub = process.new_subject()
          actor.send(a, Fibonacci(47, sub))
          sub
        }
        Error(_) -> {
          panic
        }
      }
    })

  list.each(subjects, fn(subject) {
    let assert Ok(result) = process.receive(subject, 60_000)
    io.debug(result)
  })
}

type Message {
  Factorial(base: Int, reply_with: process.Subject(Result(Int, Nil)))
  Fibonacci(n: Int, reply_with: process.Subject(Result(Int, Nil)))
}

fn handler(message: Message, _data: Nil) -> actor.Next(Message, Nil) {
  case message {
    Factorial(base, client) -> {
      process.send(client, Ok(factorial(base, 1)))
      actor.continue(Nil)
    }
    Fibonacci(n, client) -> {
      process.send(client, Ok(fib(n)))
      actor.continue(Nil)
    }
  }
}

fn factorial(base: Int, acc: Int) {
  case base {
    0 -> acc
    _ -> factorial(base - 1, acc * base)
  }
}

fn fib(n: Int) {
  case n {
    res if res < 2 -> res
    _ -> fib(n - 1) + fib(n - 2)
  }
}
